import logo from "./logo.svg";
import "./App.css";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Spinner from "components/spiner/Spinner";
import { routes } from "Routes/routes";
// form antd
function App() {
  return (
    <div className="App">
      {/* <Spinner /> */}
      <BrowserRouter>
        <Routes>
          {routes.map(({ path, component }, index) => {
            return <Route key={index} path={path} element={component} />;
          })}
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
